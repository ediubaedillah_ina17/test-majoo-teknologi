@extends('layouts.app')

@section('main')
<div class="main">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="text-center">
                    <h3>Form Input Kategori</h3>
                </div>
                <form id="form-createCategory" method="POST">
                    <div class="form-group">
                        <label for="name">Nama Kategori</label>
                        <input type="text" name="name" class="form-control" id="name" aria-describedby="name" placeholder="Nama Produk">
                    </div>
                    <button type="submit" class="btn btn-primary simpan">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('#form-createCategory').submit(function(e){
        e.preventDefault();
        $('.simpan').html('Simpan...');
        $('.simpan').attr('disabled', 'disabled');
        var formDataCreateCategory = new FormData(this);
        $.ajax({
            type: "POST",
            url: "http://localhost:8000/api/categories",
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${localStorage.getItem("token")}`);
            },
            data: formDataCreateCategory,
            success: function(data) {
                alert('Success save category!');
                window.location.href = 'http://localhost:8000/product';
            },
            error: function(data) {
                alert(data.responseJSON.message);
                $('.simpan').html('Simpan');
                $('.simpan').removeAttr('disabled', false);
            }
        });
    });
</script>
@endsection
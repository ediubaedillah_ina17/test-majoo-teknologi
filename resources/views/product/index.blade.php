@extends('layouts.app')

@section('main')
<div class="main mb-4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 style="font-weight: bold;">Produk</h2>
            </div>
        </div>
        <div class="row" id="product-content">
        </div>
        {{-- <div class="row">
            <div class="col-md-12 col-sm-12">
                <nav aria-label="..." class="mt-3">
                    <ul class="pagination">
                      <li class="page-item disabled">
                        <span class="page-link">Previous</span>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item active">
                        <span class="page-link">
                          2
                          <span class="sr-only">(current)</span>
                        </span>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                      </li>
                    </ul>
                  </nav>
            </div>
        </div> --}}
    </div>
</div>
<script>
  $(document).ready(function(){
    $.ajax({
      type: "GET",
      url: "http://localhost:8000/api/products",
      beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${localStorage.getItem("token")}`);
      },
      success: function(data) {
        const products = data;
        let containerProduct = '';
        // console.log(products);
        if(products.result > 0) {
          products.data.forEach(element => {
            containerProduct += `
            <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card border border-dark mb-2">
                <img src="/products/${element.image}" class="card-img-top" alt="Paket Advance">
                <div class="card-body text-center">
                    <h5 class="card-title">${element.name}</h5>
                    <h5 class="card-text" style="font-weight: bold;">Rp. ${element.price}</h5>
                    <div class="card-text text-justify" style="min-height: 100px">
                      ${element.description}
                    </div>
                    <a href="#" class="btn btn-outline-dark">Beli</a>
                </div>
            </div>
            </div>
            `;
          });
        } else {
          // console.log('Data tidak ada')
          containerProduct = `
            <div class="col-lg-3 col-md-6 col-sm-12">
              <p>Produk masih kosong!</p>
            </div>
          `;
        }
        $('#product-content').html(containerProduct);
        console.log(products);
      },
      error: function(data) {
          alert(data.responseJSON.message);
      }
    });
  });
</script>
@endsection
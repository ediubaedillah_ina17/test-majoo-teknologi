@extends('layouts.app')

@section('main')
    <div class="main mb-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-4 col-lg-4">
                    <div class="text-center">
                        <h3>Form Input Kategori</h3>
                    </div>
                    <div class="container-fluid">
                        <form action="" method="POST" id="form-createProduct" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name">Nama Produk</label>
                                <input type="text" class="form-control" name="name" id="name" aria-describedby="name" placeholder="Nama Produk">
                            </div>
                            <div class="form-group">
                                <label for="price">Harga</label>
                                <input type="number" class="form-control" name="price" id="price" placeholder="Harga">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" id="deskripsi" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="categories">Kategori</label>
                                <select name="categories" id="categories" class="form-control">
                                    <option value=""></option>
                                    <option value="Accessories">Accessories</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="image">Upload Gambar</label>
                                <div class="input-group mb-1">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image" accept=".png,.jpg,.jpeg">
                                        <label class="custom-file-label" for="image">Choose file...</label>
                                    </div>
                                </div>
                                <progress id="progressBar" value="0" max="100" style="width:100%;"></progress>
                                <h3 id="status"></h3>
                                <p id="loaded_n_total"></p>
                            </div>
                            <button type="submit" class="btn btn-primary simpan">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <script>
        document.querySelector('.custom-file-input').addEventListener('change',function(e){
            var fileName = document.getElementById("image").files[0].name;
            var nextSibling = e.target.nextElementSibling
            nextSibling.innerText = fileName
        })
        $(document).ready(function(){
            // console.log('hello')
            $('#categories').select2({
                placeholder: 'Pilih Kategori',
                ajax: {
                    url: 'http://localhost:8000/api/categories',
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', `Bearer ${localStorage.getItem("token")}`);
                    },
                    processResults: function (data) {
                        console.log(data.data);
                    // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data.data
                        };
                    }
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                },
            });
        });

        CKEDITOR.replace( 'deskripsi' );
        // $('#kategori').select2({
        //     placeholder: 'Pilih Kategori'
        // });
        

        $('#form-createProduct').submit(function(e){
            e.preventDefault();
            $('.simpan').html('Simpan...');
            $('.simpan').attr('disabled', 'disabled');
            var formDataCreateProduct = new FormData(this);
            formDataCreateProduct.append('description', CKEDITOR.instances['deskripsi'].getData());
            $.ajax({
                type: "POST",
                url: "http://localhost:8000/api/products",
				processData: false,
				contentType: false,
				cache: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', `Bearer ${localStorage.getItem("token")}`);
                },
				enctype: 'multipart/form-data',
                data: formDataCreateProduct,
                success: function(data) {
                    alert('Success save product!');
                    window.location.href = 'http://localhost:8000/product';
                    // $('.simpan').html('Simpan');
                    // $('.simpan').removeAttr('disabled', false);
                },
                error: function(data) {
                    alert(data.responseJSON.message);
                    $('.simpan').html('Simpan');
                    $('.simpan').removeAttr('disabled', false);
                }
            });
        });

        function progressHandler(event) {
            _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
            var percent = (event.loaded / event.total) * 100;
            _("progressBar").value = Math.round(percent);
            _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
        }

        function completeHandler(event) {
            _("status").innerHTML = event.target.responseText;
            _("progressBar").value = 0; //wil clear progress bar after successful upload
        }

        function errorHandler(event) {
            _("status").innerHTML = "Upload Failed";
        }

        function abortHandler(event) {
            _("status").innerHTML = "Upload Aborted";
        }

        function uploadFile() {
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler, false);
            ajax.addEventListener("load", completeHandler, false);
            ajax.addEventListener("error", errorHandler, false);
            ajax.addEventListener("abort", abortHandler, false);
        }
    </script>
@endsection
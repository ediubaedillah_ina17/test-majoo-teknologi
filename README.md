Untuk menjalankan projek ini, silahkan ikuti langkah berikut

1. Buat database
2. Ubah file .env.example menjadi .env
3. Masukan konfigurasi database ke .env
4. Jalankan php artisan migrate
5. Jalankan php artisan db:seed
6. Username: admin
7. Password: password

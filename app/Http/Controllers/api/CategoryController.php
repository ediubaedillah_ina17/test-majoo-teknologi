<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->user = auth()->guard('api')->user();
    } //end __construct()

    public function index()
    {
        try {
            $categories = Category::all('id', 'name as text');
            return response([
                'status' => 'success',
                'data' => $categories,
            ], 200, []);
        } catch (\Throwable $th) {
            return response([
                'status' => 'failed',
                'message' => 'Error get data!',
            ], 400, []);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response([
                'status' => 'failed',
                'message' => 'Please fill all column input!',
            ], 400, []);
        }

        try {
            Category::create($request->all());
            return response([
                'status' => 'success',
                'data' => $request->all(),
            ], 200, []);
        } catch (\Throwable $th) {
            return response([
                'status' => 'failed',
                'message' => 'Error save data!',
            ], 400, []);
        }
    }
}

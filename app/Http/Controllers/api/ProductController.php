<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->user = auth()->guard('api')->user();
    } //end __construct()

    public function index()
    {
        try {
            $products = Product::all();
            return response([
                'status' => 'success',
                'result' => count($products),
                'data' => $products,
            ], 200, []);
        } catch (\Throwable $th) {
            return response([
                'status' => 'failed',
                'message' => 'Error get data!',
            ], 400, []);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'categories' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        if ($validator->fails()) {
            return response([
                'status' => 'failed',
                'message' => 'Please fill all column input!',
            ], 400, []);
        }
        $productRequest = $request->all(['name', 'price', 'description', 'categories']);

        $DS = DIRECTORY_SEPARATOR;
        $dir = public_path() . $DS . 'products' . $DS;
        $objFile = $request->file('image');
        $filename = 'product_' . date('YmdHis') . rand(100, 999) . '.' . $objFile->extension();

        if (!File::isDirectory($dir)) {
            File::makeDirectory($dir, 0777, true, true);
        }
        $objFile->move($dir, $filename);
        try {
            $user = auth()->user();
            $productRequest['image'] = $filename;
            $productRequest['created_by'] = $user->id;
            $productRequest['categories'] = Category::whereId($request->categories)->first()->name;
            Product::create($productRequest);
            return response([
                'status' => 'success',
                'data' => $productRequest,
            ], 200, []);
        } catch (Exception $e) {
            Log::debug(json_encode($e));
            return response([
                'status' => 'failed',
                'message' => 'Error save product!',
            ], 400, []);
        }
    }
}
